package com.markgen.adapter;

import org.apache.commons.lang3.RandomUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class SceneBlockAdapter {

    private JSONObject nexus;
    //private JSONArray block;
    private List<Object> blockList;

    private Map<String, String> currentBlock;

    public SceneBlockAdapter(JSONObject nexus) {
        this.nexus = nexus;
        this.blockList = nexus.getJSONArray("block").toList();
    }

    public boolean hasBlocks() {
        return !this.blockList.isEmpty();
    }

    public SceneBlockAdapter next() {
        this.currentBlock = (Map<String, String>) this.blockList.remove(0);
        return this;
    }

    public String getCharacterName() {
        return this.currentBlock.get("character");
    }

    public boolean isRando() {
        return this.currentBlock.get("character").equals("rando");
    }

    public boolean isAnyCharacter() {
        return getCharacterName().equals("any-named");
    }

    public String getStartingWord() {
        return this.currentBlock.get("starting-word");
    }

    public boolean isAnyStartingWord() {
        return getStartingWord().equals("*");
    }

    public boolean hasMinMax() {
        return this.currentBlock.containsKey("min")
                && this.currentBlock.containsKey("max");
    }

    public int getMin() {
        return getRandomInteger("min");
    }

    public int getMax() {
        return getRandomInteger("max");
    }

    private int getRandomInteger(String intPattern) {
        String[] minPattern = this.currentBlock.get(intPattern).split("-");

        int b = Integer.parseInt(minPattern[0]);
        int e = Integer.parseInt(minPattern[1]);

        return RandomUtils.nextInt(b, e + 1);
    }




}
