package com.markgen.model;

import java.awt.image.BufferedImage;

public class FinalSceneBlock {

    private BufferedImage image;
    private int order;


    public FinalSceneBlock(BufferedImage image, int order) {
        this.image = image;
        this.order = order;
    }

    public int getHeight() {
        return this.image.getHeight();
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
