package com.markgen.service;

import com.markgen.dto.AvengerQuoteResponse;
import com.markgen.dto.AvengerQuoteRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Mono;

@Service
public class QuoteService extends AbstractApiService {

    @Value("${api.url.quote-engine}")
    private String endpoint;


    public String getAvengerQuote(String character, int min, int max, String startingWord) {

        AvengerQuoteRequest params = new AvengerQuoteRequest();
        params.setCharacter(character);
        params.setMaxCharacters(max);
        params.setMinCharacters(min);
        params.setStartingWord(startingWord);

        Mono<AvengerQuoteResponse> response =  this.webClient.post()
                .body(Mono.just(params), AvengerQuoteRequest.class)
                .retrieve()
                .bodyToMono(AvengerQuoteResponse.class);


        // The quote engine will occasionally not return a value if it cannot markovify with a given
        //  starting word
        AvengerQuoteResponse toReturn = response.blockOptional().orElse( new AvengerQuoteResponse());

        return toReturn.getQuote();

    }

    @Override
    protected String getEndpoint() {
        return this.endpoint;
    }
}
