package com.markgen.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import javax.annotation.PostConstruct;

public abstract class AbstractApiService {

    @Value("${wiretap}")
    private boolean isWiretapEnabled;

    protected WebClient webClient;

    protected abstract String getEndpoint();

    @PostConstruct
    void init() {
        this.webClient = WebClient.builder()
                .baseUrl(getEndpoint())
                .clientConnector(new ReactorClientHttpConnector(
                        HttpClient.create().wiretap(isWiretapEnabled)
                ))
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

}
