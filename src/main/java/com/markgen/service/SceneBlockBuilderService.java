package com.markgen.service;

import com.markgen.model.FinalSceneBlock;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SceneBlockBuilderService {

    public byte[] buildScene(List<FinalSceneBlock> sceneBlocks) {

        int totalHeight = sceneBlocks.stream()
                .map(FinalSceneBlock::getHeight)
                .reduce(0, Integer::sum);

        BufferedImage result = new BufferedImage(1028, totalHeight, BufferedImage.TYPE_INT_RGB);
        Graphics graphics = result.getGraphics();

        int x = 0, y = 0;

        List<FinalSceneBlock> sorted = sceneBlocks.stream()
                .sorted( (sb1, sb2) -> sb1.getOrder() > sb2.getOrder() ? 1 : 0 )
                .collect(Collectors.toList());

        for (FinalSceneBlock sceneBlock : sorted) {
            graphics.drawImage(sceneBlock.getImage(), x, y, null);
            y += sceneBlock.getHeight();
        }


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(result, "jpg", outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return outputStream.toByteArray();
    }
}
