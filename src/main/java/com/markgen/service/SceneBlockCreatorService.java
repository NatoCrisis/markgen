package com.markgen.service;

import com.markgen.dto.SceneBlockTemplate;
import com.markgen.model.FinalSceneBlock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class SceneBlockCreatorService {

    Logger LOG = LoggerFactory.getLogger(SceneBlockCreatorService.class);

    @Autowired
    private QuoteService quoteService;

    @Autowired
    private ImageService imageService;


    public FinalSceneBlock createSceneBlock(final SceneBlockTemplate template) {
        FinalSceneBlock toReturn = null;

        CompletableFuture<BufferedImage> generatedImage = CompletableFuture
                .supplyAsync(() -> {
                    if (template.isRando()) {
                        return "";
                    } else {
                        return quoteService
                                .getAvengerQuote(template.getCharacterName(), template.getMinCharacters(),
                                        template.getMaxCharacters(), template.getStartingWord());
                    }
                })
                .thenApply(quote -> imageService.createImage(template.getCharacterName(), quote));

        try {
            toReturn = new FinalSceneBlock(generatedImage.get(), template.getSceneNumber());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return toReturn;
    }
}
