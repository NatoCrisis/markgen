package com.markgen.service;

import com.markgen.dto.CharacterImageRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

@Service
public class ImageService extends AbstractApiService {

    @Value("${api.url.image-engine}")
    private String endpoint;

    public BufferedImage createImage(String characterName, String quote) {
        CharacterImageRequest request = new CharacterImageRequest(characterName, quote);

        return callEndpoint(request);
    }

    private BufferedImage callEndpoint(CharacterImageRequest request) {
        System.out.println(request);
        Mono<String> resp = this.webClient.post()
                .body(Mono.just(request), CharacterImageRequest.class)
                .retrieve()
                .bodyToMono(String.class);

        String stringRepresentation = resp.block();

        return toBufferedImage(toInputStream(stringRepresentation));
    }

    private BufferedImage toBufferedImage(InputStream inputStream) {
        BufferedImage toReturn = null;
        try {
            toReturn = ImageIO.read(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return toReturn;
    }

    private InputStream toInputStream(String imageRepresentation) {
        // System.out.println(imageRepresentation);
        byte[] imageAsBytes = Base64.getDecoder().decode(imageRepresentation);
        return new ByteArrayInputStream(imageAsBytes);
    }

    @Override
    protected String getEndpoint() {
        return this.endpoint;
    }
}
