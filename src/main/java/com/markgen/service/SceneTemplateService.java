package com.markgen.service;


import com.markgen.adapter.SceneBlockAdapter;
import com.markgen.dto.SceneBlockTemplate;
import org.apache.commons.lang3.RandomUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.*;
;
import java.util.stream.Collectors;

@Service
public class SceneTemplateService {


    @Value("classpath:characters")
    Resource characterFile;

    @Value("classpath:scene-block.json")
    Resource sceneTemplate;

    private List<String> characters;
    private JSONObject sceneTemplates;


    @PostConstruct
    void init() {
        try {
            readCharacters();
            readSceneTemplate();
        } catch (IOException e) {
            // TODO:: figure out what do do here
            e.printStackTrace();
        }
    }

    public List<SceneBlockTemplate> getSceneTemplate() {
        List<String> randomCharacters = getRandomCharacters(4);
        SceneBlockAdapter adapter = new SceneBlockAdapter(getRandomNexus());

        List<SceneBlockTemplate> fullScene = new LinkedList<>();
        int order = 0;
        while (adapter.hasBlocks()) {
            SceneBlockTemplate sb = new SceneBlockTemplate();
            adapter.next();

            if (adapter.isAnyCharacter()) {
                sb.setCharacterName(randomCharacters.remove(0));
            } else {
                sb.setCharacterName("rando");
            }

            if (!adapter.isAnyStartingWord()) {
                sb.setStartingWord(adapter.getStartingWord());
            }

            if (adapter.hasMinMax()) {
                sb.setMinCharacters(adapter.getMin());
                sb.setMaxCharacters(adapter.getMax());
            }

            sb.setSceneNumber(order++);

            fullScene.add(sb);
        }

        return fullScene;
    }

    private JSONObject getRandomNexus() {
        int totalBlocks = sceneTemplates.getInt("total-blocks");
        int randomNexus = RandomUtils.nextInt(1, totalBlocks + 1);

        return sceneTemplates.getJSONObject("nexus-" + randomNexus);
    }

    private List<String> getRandomCharacters(int limit) {
        List<String> copy = new ArrayList<>(characters);

        Collections.shuffle(copy);
        return copy.stream().limit(limit).collect(Collectors.toList());
    }

    private void readSceneTemplate() throws IOException {
        String json = "";
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(sceneTemplate.getInputStream()))) {
            json = reader.lines().collect(Collectors.joining());
        }
        sceneTemplates = new JSONObject(json);
    }

    private void readCharacters() throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(characterFile.getInputStream()))) {
            characters = reader.lines().collect(Collectors.toList());
        }
        // Does not work when running in Docker (specifically Paths does not play well)
//        try(BufferedReader reader = Files.newBufferedReader(Paths.get(characterFile.getURI()))) {
//            characters = reader.lines().collect(Collectors.toList());
//        }
    }


}
