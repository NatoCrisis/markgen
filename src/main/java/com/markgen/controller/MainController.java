package com.markgen.controller;

import com.markgen.dto.SceneBlockTemplate;
import com.markgen.model.FinalSceneBlock;
import com.markgen.service.SceneBlockBuilderService;
import com.markgen.service.SceneBlockCreatorService;
import com.markgen.service.SceneTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;


@RestController
@RequestMapping("main")
public class MainController {

    @Autowired
    private SceneTemplateService sceneTemplateService;

    @Autowired
    private SceneBlockCreatorService sceneBlockCreatorService;

    @Autowired
    private SceneBlockBuilderService sceneBlockBuilderService;


    @GetMapping(value = "index", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody byte[] index() {

        List<SceneBlockTemplate> fullScene = sceneTemplateService.getSceneTemplate();

        List<CompletableFuture<FinalSceneBlock>> blocks =  fullScene.stream()
                .map(template -> CompletableFuture.supplyAsync(() -> template))
                .map(future -> future.thenApply(template -> sceneBlockCreatorService.createSceneBlock(template)))
                .collect(Collectors.toList());


        List<FinalSceneBlock> sceneBlocks = blocks.stream()
                .map(b -> {
                    try {
                        return b.get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());

        return sceneBlockBuilderService.buildScene(sceneBlocks);
    }
}
