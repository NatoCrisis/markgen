package com.markgen.dto;

public class CharacterImageRequest {

    private String character;
    private String text;

    public CharacterImageRequest(String character, String text) {
        this.character = character;
        this.text = text;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "CharacterImageRequest{" +
                "character='" + character + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
