package com.markgen.dto;

public class AvengerQuoteResponse {
    private String quote;

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }
}
