package com.markgen.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AvengerQuoteRequest {

    private String character;

    @JsonProperty("max_characters")
    private int maxCharacters;

    @JsonProperty("min_characters")
    private int minCharacters;

    @JsonProperty("starting_word")
    private String startingWord;

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public int getMaxCharacters() {
        return maxCharacters;
    }

    public void setMaxCharacters(int maxCharacters) {
        this.maxCharacters = maxCharacters;
    }

    public int getMinCharacters() {
        return minCharacters;
    }

    public void setMinCharacters(int minCharacters) {
        this.minCharacters = minCharacters;
    }

    public String getStartingWord() {
        return startingWord;
    }

    public void setStartingWord(String startingWord) {
        this.startingWord = startingWord;
    }
}
