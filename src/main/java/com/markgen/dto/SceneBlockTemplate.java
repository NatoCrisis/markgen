package com.markgen.dto;

public class SceneBlockTemplate {

    private String characterName;
    private String startingWord;
    private int minCharacters;
    private int maxCharacters;
    private int sceneNumber;

    public boolean isRando() {
        return "rando".equals(this.characterName);
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getStartingWord() {
        return startingWord;
    }

    public void setStartingWord(String startingWord) {
        this.startingWord = startingWord;
    }

    public int getMinCharacters() {
        return minCharacters;
    }

    public void setMinCharacters(int minCharacters) {
        this.minCharacters = minCharacters;
    }

    public int getMaxCharacters() {
        return maxCharacters;
    }

    public void setMaxCharacters(int maxCharacters) {
        this.maxCharacters = maxCharacters;
    }

    public int getSceneNumber() {
        return sceneNumber;
    }

    public void setSceneNumber(int sceneNumber) {
        this.sceneNumber = sceneNumber;
    }

    @Override
    public String toString() {
        return "SceneBlockTemplate{" +
                "characterName='" + characterName + '\'' +
                ", startingWord='" + startingWord + '\'' +
                ", minCharacters=" + minCharacters +
                ", maxCharacters=" + maxCharacters +
                ", sceneNumber=" + sceneNumber +
                '}';
    }
}
