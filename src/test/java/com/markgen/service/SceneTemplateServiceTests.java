package com.markgen.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.markgen.dto.SceneBlockTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
public class SceneTemplateServiceTests {


    @Autowired
    SceneTemplateService service;

    @Test
    public void testSceneGen() {
        List<SceneBlockTemplate> list = service.getSceneTemplate();
        assertThat(list.size() == 4);

    }
}
